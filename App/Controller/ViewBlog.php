<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 25/2/19
 * Time: 2:30 PM
 */

/**
 *
 * Class is handling the request related to view blog
 *
 * */
class ViewBlog extends Controller
{
    /**
     * This function setup the view of blog
     *
     * */
    public function Index()
    {
        if ('GET' == $_SERVER['REQUEST_METHOD'])
        {
            if (!empty($_GET['Id']))
            {
                if ( empty($this->ModelObj['Post']) )
                {
                    $this->Model('Post');
                }
                $this->Model('Comment');
                $Data = $this->ModelObj['Post']->SearchPost(
                    array('Id', 'UserId', 'Title', 'Content', 'Modified', 'UpVotes', 'DownVotes'),
                    array('Id'),
                    array('Id' => $_GET['Id']),
                    array()
                );
                $CommentData = $this->ModelObj['Comment']->GetComment($_GET['Id']);
                if ( empty($this->ModelObj['Utility']) )
                {
                    $this->Model('Utility');
                }
                $this->Model('DatabaseHandling');
                if (is_array($Data) && count($Data) && is_array($CommentData) )
                {

                    $PostData = $Data[0];
                    $PostData['AuthorName'] = $this->ModelObj['Utility']->GetAuthor($PostData['UserId']);
                    $IsLogIn = 0;
                    if ( $this->ModelObj['Utility']->IsLogin(0) || $this->ModelObj['Utility']->IsLogin(1) )
                    {
                        $IsLogIn = 1;
                    }

                    foreach ( $CommentData as $Row => $Value )
                    {
                        $CommentData[$Row]['UserName'] = $this->ModelObj['Utility']->GetAuthor($Value['UserId']);
                    }

                    $this->View('Header',['IsLogInvar',$IsLogIn]);
                    $this->View('Blog/ViewBlog',['PostData' => $PostData,'Comment' => $CommentData]);
                }
                else
                {
                    header('Location: /View/index.php?Err=Some error occurred!! Please try again.');
                }
            }
            else
            {
                header('Location: /View/index.php?Err=Some error occurred!! Please try again.');
            }
        }
        else
        {
            header('Location: /View/index.php?Err=Some error occurred!! Please try again.');
        }
    }


}

