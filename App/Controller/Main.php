<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 26/2/19
 * Time: 10:50 AM
 */

class Main  extends Controller
{
    public function Index()
    {

        $this->Model('Post');
        $this->Model('Utility');

        $Result = $this->ModelObj['Post']->GetLatestPosts();
        $IsLogin = 1;
        if ( empty( $_SESSION['UserId'] ))
        {
            $IsLogin = 0;
        }
        foreach ($Result as $Row => $Value)
        {
            $Result[$Row]['AuthorName'] = $this->ModelObj['Utility']->GetAuthor($Value['UserId']);
        }
        $Message = '';
        $Err = '';
        if (!empty($_GET['Message']))
        {
            $Message = $_GET['Message'];
        }
        if (!empty($_GET['Err']))
        {
            $Err = $_GET['Err'];
        }
        $this->View('Header',['IsLogInVar' => $IsLogin]);
        $this->View('Index',['Result' => $Result,'Err'=>$Err,'Message'=>$Message]);

    }
}