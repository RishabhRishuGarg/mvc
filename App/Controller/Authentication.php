<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/ErrorCode.php';
/**
 * Class is handling the functionality related to authentication
 *
 * @package Model
 * */
class Authentication extends Controller
{

    private $IsAuthor = 0;

    /**
     * This function is responsible of getting the default view of authentication
     *
     */
    public function Index()
    {
        $Message = '';
        $Err = '';
        if (!empty($_GET['Message']))
        {
            $Message = $_GET['Message'];
        }
        if (!empty($_GET['Err']))
        {
            $Err = $_GET['Err'];
        }

        if ( empty($this->ModelObj['Utility']) )
        {
            $this->Model('Utility');
        }
        if ( $this->ModelObj['Utility']->IsLogin(0) || $this->ModelObj['Utility']->IsLogin(1) )
        {

            header('Location: /Profile/Index?Err='.$Err.'&Message='.$Message);
        }
        else
        {
            $this->Login();
            /*$this->View('Header',['IsLogInVar' => 0]);
            $this->View( 'User/Login',['Err'=>$Err,'Message'=>$Message]);*/
        }
    }

    /**
     *  Function is responsible of requesting model to login and send the data to view
     *
     */
    public function Login()
    {
        $LogInURL = $this->GoogleClient->createAuthUrl();
        $this->Model('Utility');
        if ( $this->ModelObj['Utility']->IsLogin(0) || $this->ModelObj['Utility']->IsLogin(1) )
        {
            header('Location: /Profile');
        }
        if ('POST' == $_SERVER['REQUEST_METHOD'])
        {
            $Required=array( 'UserId', 'Password' );
            if ( empty($this->ModelObj['Validation']) )
            {
                $this->Model('Validation');
            }
            $Status =$this->ModelObj['Validation']->IsEmpty($Required);

            if ( ErrorCode::EverythingOkay == $Status )
            {
                if ( empty($this->ModelObj['User']) )
                {
                    $this->Model('User');
                }
                if(!empty($_POST['IsAuthor']) && $_POST['IsAuthor'])
                {
                    $this->IsAuthor = 1;
                }

                $Status = $this->ModelObj['User'] ->Login($_POST['UserId'], $_POST['Password'], $this->IsAuthor);
                if ( empty($this->ModelObj['ErrorCode']) )
                {
                    $this->Model('ErrorCode');
                }
                if ( ErrorCode::EverythingOkay == $Status )
                {
                    header('Location: /');
                }
                else
                {
                    $this->View('Header',['IsLogInVar' => 0]);
                    $this->View(
                        'User/Login',
                        [
                            'Err'=>$this->ModelObj['ErrorCode']->GetErrorDetail($Status),
                            'LogInURL' => $LogInURL
                        ]
                    );

                }
            }
            else
            {
                $this->View('Header',['IsLogInVar' => 0]);
                $this->View('User/Login',['Err'=>$Status,'LogInURL' => $LogInURL]);
            }

        }
        else
        {
            $Message = '';
            $Err = '';
            if (!empty($_GET['Message']))
            {
                $Message = $_GET['Message'];
            }
            if (!empty($_GET['Err']))
            {
                $Err = $_GET['Err'];
            }
            $this->View('Header',['IsLogInVar' => 0]);
            $this->View('User/Login',['Message'=>$Message,'Err'=>$Err,'LogInURL' => $LogInURL]);
        }
    }

    /**
     *  Function just destroys the session to log out
     *
     */
    public function Logout()
    {
        session_destroy();
        header('Location: /Authentication/Login');
    }


    /**
     * This function implements the SignUp functionality
     * This function accept request and create the view according to request
     *
     * */
    public function SignUp()
    {
        if ( empty($this->ModelObj['Utility']) )
        {
            $this->Model('Utility');
        }
        if ( $this->ModelObj['Utility']->IsLogin(1) || $this->ModelObj['Utility']->IsLogin(0) )
        {
            header('Location: /Profile/Index?Err=You have to logout first to SignUp');
        }
        if ( 'POST' == $_SERVER['REQUEST_METHOD'] )
        {
            $Err='';
            $Flag=1;
            $IsAuthor = 0;


            $Required=array(
                'FirstName' ,
                'LastName' ,
                'Email' ,
                'Password' ,
                'NickName' ,
                'Address' ,
                'Country' ,
                'State' ,
                'City' ,
                'Pincode' ,
                'PhoneNumber' ,
                'RePassword'
            );
            if ( empty( $this->ModelObj['Validation'] ) )
            {
                $this->Model('Validation');
            }
            $Status = $this->ModelObj['Validation']->IsEmpty( $Required );
            if( ErrorCode::EverythingOkay == $Status )
            {
                if ( !($_POST['Password'] == $_POST['RePassword']) )
                {
                    $Flag = 0;
                    $Err = 'Password Does Not Match. Please try again';
                }
                elseif (
                    !$this->ModelObj['Validation']->NameCheck($_POST['FirstName']) ||
                    !$this->ModelObj['Validation']->NameCheck($_POST['LastName']) )
                {
                    $Flag = 0;
                    $Err = ' Only letters and white space allowed in Name';
                }

                else if ( !$this->ModelObj['Validation']->EmailCheck( $_POST['Email'] ) )
                {
                    $Flag = 0;
                    $Err = 'Invalid email format';
                }
                else if (!$this->ModelObj['Validation']->NameCheck($_POST['NickName']))
                {
                    $Flag = 0;
                    $Err = 'Only letters and white space allowed in Nick Name';
                }
                else if (!$this->ModelObj['Validation']->NameCheck($_POST['Country']))
                {
                    $Flag = 0;
                    $Err = 'Only letters and white space allowed in Country';
                }
                else if (!$this->ModelObj['Validation']->NameCheck($_POST['State']))
                {
                    $Flag = 0;
                    $Err = 'Only letters and white space allowed in State';
                }
                else if (!$this->ModelObj['Validation']->NameCheck($_POST['City']))
                {
                    $Flag = 0;
                    $Err = 'Only letters and white space allowed in City';
                }
                else if ( !empty($_POST['IsAuthor']) && 1 == $_POST['IsAuthor'] )
                {
                    $IsAuthor = 1;
                }
                $SignUpFormData = array(
                    'Address' => $_POST['Address']
                );
                if ( empty( $this->ModelObj['Sanitize'] ) )
                {
                    $this->Model('Sanitize');
                }
                $this->ModelObj['Sanitize']->Sanitization($SignUpFormData);
            }
            if (1 == $Flag)
            {
                if ( empty( $this->ModelObj['ErrorCode'] ) )
                {
                    $this->Model('ErrorCode');
                }
                if ( empty( $this->ModelObj['User'] ) )
                {
                    $this->Model('User');
                }
                $StatusValue = $this->ModelObj['User']->Signup(
                    $_POST['FirstName'],
                    $_POST['LastName'],
                    $_POST['Email'],
                    password_hash($_POST['Password'], PASSWORD_BCRYPT, array('cost' => 12)),
                    $_POST['NickName'],
                    $IsAuthor,
                    $SignUpFormData['Address'],
                    $_POST['Country'],
                    $_POST['State'],
                    $_POST['City'],
                    $_POST['Pincode'],
                    $_POST['PhoneNumber']
                );
                if (ErrorCode::EverythingOkay == $StatusValue)
                {
                    header('Location: /Authentication/Login?Message=SignUp Successfully');
                }
                elseif (ErrorCode::EmailExist == $StatusValue)
                {
                    header('Location: /Authentication/Signup?Err=Email already exist try with another email!');
                }
                elseif (ErrorCode::PhoneNumberExist == $StatusValue)
                {
                    header('Location: /Authentication/Signup?Err=Phone Number Already Exists Try another Phone Number');
                }
            }
            else
            {
                header('Location: /View/User/SignupView.php?Err='.$Err);
            }
        }
        else
        {
            $Message = '';
            $Err = '';
            if (!empty($_GET['Message']))
            {
                $Message = $_GET['Message'];
            }
            if (!empty($_GET['Err']))
            {
                $Err = $_GET['Err'];
            }
            $this->View('Header',['IsLogInVar'=>0]);
            $this->View('User/Signup',['Err'=>$Err,'Message'=>$Message]);
        }

    }
}