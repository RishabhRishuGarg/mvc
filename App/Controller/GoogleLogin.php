<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 1/3/19
 * Time: 12:16 PM
 */


/**
 *
 * This class is responsible for handling the request of google login feature
 *
 * @package Controlller
 * */
class GoogleLogin extends Controller
{

    /**
     * Function is responsible for handling the request to log in with google
     *
     * @return void
     * */
    public function Index()
    {
        if ( isset( $_SESSION['AccessToken'] ) )
        {
            $this->GoogleClient->setAccessToken($_SESSION['AccessToken']);
        }
        elseif ( isset( $_GET['code'] ) )
        {
            $Token = $this->GoogleClient->fetchAccessTokenWithAuthCode($_GET['code']);
            $_SESSION['AccessToken'] = $Token;
        }
        else
        {
            header( 'Location: Authentication/Login?Err=Error occurred during login please try again ');
        }
        $OAuth = new Google_Service_Oauth2($this->GoogleClient);
        $UserData = $OAuth->userinfo_v2_me->get();
        $Email              = $UserData['email'];
        $FirstName          = $UserData['givenName'];
        $LastName           = $UserData['familyName'];
        $this->Model('User');
        $this->Model('Utility');
        if( $this->ModelObj['Utility']->IsExist(
            UserTableName,
            array('Email'),
            array('Email' => $Email)
        ))
        {
            $_SESSION['UserId'] = $Email;
            header('Location: /');
        }
        else
        {
            $this->Model('ErrorCode');
            $Status = $this->ModelObj['User']->Signup($FirstName, $LastName, $Email);
            if ( ErrorCode::EverythingOkay == $Status )
            {
                header('Location: Authentication/Login?Message=You have been Logged in Successfully');
            }
            else
            {
                header('Location: Authentication/Login?Message=Some Error occurred please Try again');
            }
        }
    }
}