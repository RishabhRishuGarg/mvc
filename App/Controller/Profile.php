<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 27/2/19
 * Time: 12:04 PM
 */


/**
 * Class is only responsible of operation of profile
 *
 * @package Controller
 * */

class Profile extends Controller
{

    /**
     * Function is handling the functionalists of profile This function taking the data of user profile and sending
     * it to the view part.
     * @return void
     * */
    public function Index()
    {

        $Message = '';
        $Err = '';
        if ( !empty($_GET['Message']) )
        {
            $Message = $_GET['Message'];
        }
        if ( !empty($_GET['Err']) )
        {
            $Err = $_GET['Err'];
        }
        if(empty( $this->ModelObj['Utility'] ) )
        {
            $this->Model('Utility');
        }
        if ( $this->ModelObj['Utility']->IsLogin(0) || $this->ModelObj['Utility']->IsLogin(1))
        {
            $this->View('Header',['IsLogInVar' => 1]);
            $ProfileData = $this->ModelObj['Utility']->GetDataForProfile();
            $this->View('Profile',['Data' => $ProfileData,'Message' => $Message, 'Err'=>$Err ] );
        }
        else
        {
            header('Location: Authentication/Index?Message=You must have to login to view profile');
        }

    }
}