<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 28/2/19
 * Time: 12:13 PM
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/App/Utilities/ErrorCode.php';

class AddComment extends Controller
{
    public function Index()
    {
        if ( 'POST' == $_SERVER['REQUEST_METHOD'] )
        {
            $this->Model('Comment');
            $this->Model('Validation');
            $this->Model('Sanitize');
            $this->Model('Utility');
            $this->Model('ErrorCode');

            $Required = array('UserId', 'PostId', 'Comment');
            $Status = $this->ModelObj['Validation']->IsEmpty($Required);
            if ( ErrorCode::EverythingOkay ==  $Status)
            {
                $FormData = array('Comment',$_POST['Comment']);
                $this->ModelObj['Sanitize']->Sanitization($FormData);
                $StatusValue = $this->ModelObj['Comment']->AddComment(
                    $_POST['PostId'],
                    $_POST['Comment']
                );
                if ( ErrorCode::EverythingOkay == $StatusValue )
                {
                    header(
                        'Location: /BlogCrud/Search?
                        Id='.$_POST['PostId'].'&
                        Message= Comment Added successfully'
                    );
                }
                else
                {
                    header(
                        'Location: /BlogCrud/Search?
                        Id='.$_POST['PostId'].'&
                        Err='.$this->ModelObj['ErrorCode']->GetErrorDetail($StatusValue)
                    );
                }
            }
            else
            {
                header('Location: /BlogCrud/Search?Id='.$_POST['PostId'].'&Err='.$Status);
            }

        }
        else
        {
            header('Location: /Main/Index?Err=Some Error occurred Please try after some time');
        }
    }
}