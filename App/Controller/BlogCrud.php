<?php

/**
 *
 * Clas is responsible of operation on Blog like Add Blog, ModifyBlog, Delete Blog
 *
 * */

class BlogCrud extends Controller
{


    /**
     * This function create the default view for this class
     *
     *
     * */
    public function Index()
    {

        if(empty( $this->ModelObj['Utility'] ) )
        {
            $this->Model('Utility');
        }
        $IsLogin = 0;
        if ( $this->ModelObj['Utility']->IsLogin(0) || $this->ModelObj['Utility']->IsLogin(1) )
        {
            $IsLogin = 1;
        }
        else
        {
            header('Location: Authentication/Login');
        }
        $this->View('Header',['IsLogInVar'=> $IsLogin]);
        $ProfileData = $this->ModelObj['Utility']->GetDataForProfile();

        $this->View('Profile',['Data'=>$ProfileData]);
    }

    /**
     *  Function is handling the operation of adding the blog
     *  It accept the request of adding blog and generate the view according to request after applying all
     *  validation and authorization
     *
     *  @return void
     */
    public function AddBlog()
    {
        if(empty( $this->ModelObj['ErrorCode'] ) )
        {
            $this->Model('ErrorCode');
        }
        if(empty( $this->ModelObj['Sanitize'] ) )
        {
            $this->Model('Sanitize');
        }
        if(empty( $this->ModelObj['Validation'] ) )
        {
            $this->Model('Validation');
        }
        if(empty( $this->ModelObj['Utility'] ) )
        {
            $this->Model('Utility');
        }
        if ( !$this->ModelObj['Utility']->IsLogin(1) )
        {

            header('Location:/Authentication/Login?Message=You Must have to login as author to add new Post');
        }
        else if ('POST' == $_SERVER['REQUEST_METHOD'])
        {
            $Required = array('Title', 'Content');
            $Status = $this->ModelObj['Validation']->IsEmpty($Required);
            if (!ErrorCode::EverythingOkay == $Status)
            {
                $this->View('Blog/AddBlogForm',['Title'=>$_POST['Title'],'Content'=>$_POST['Title']]);
            }

            if ( empty( $this->ModelObj['Author'] ) )
            {
                $this->Model('Author');
            }

            $BlogData = array(
                'Content' => $_POST['Content'],
                'Title' => $_POST['Title']
            );
            $this->ModelObj['Sanitize']->Sanitization($BlogData);
            $Status = $this->ModelObj['Author']->Write($BlogData);
            if ($Status == ErrorCode::EverythingOkay)
            {
                header('Location: /Profile/Index?Message=Blog is added successfully');
            }
            else
            {
                $this->View( 'Blog/AddBlogForm',
                    [
                        'Err'=>$this->ModelObj['ErrorCode']->GetErrorDetail($Status),
                        'Title'=>$_POST['Title'],
                        'Content'=>$_POST['Content']
                    ]
                );
            }
        }
        else
        {
            $IsLogin = 0;
            if ( $this->ModelObj['Utility']->IsLogin(0) || $this->ModelObj['Utility']->IsLogin(1))
            {
                $IsLogin = 1;
            }
            $this->View('Header',['IsLogInVar'=>$IsLogin]);
            $this->View('Blog/AddBlogForm',['Title'=>'','Content'=>'']);
        }
    }

    /**
     *  Function Handling the Modify Blog operations
     *  It accept request and create the view according to request after authorization and validations
     *
     */
    public function ModifyBlog()
    {
        $this->Model('Utility');
        if ( !$this->ModelObj['Utility']->IsLogin(1) )
        {
            $this->View('index',['Message'=>'You must be log in as author to Modify a blog ']);
        }
        elseif ( 'POST' == $_SERVER['REQUEST_METHOD'] )
        {
            $this->Model('Validation');
            $Required = array( "Content", "Id" );
            $Status = $this->ModelObj['Validation']->IsEmpty($Required);
            if ( is_int( $Status ) )
            {
                $this->Model('Author');
                $this->Model('Sanitize');
                $this->Model('ErrorCode');
                $BlogFormData = array( 'Content' => $_POST["Content"] );

                $this->ModelObj['Sanitize']->Sanitization($BlogFormData);
                $StatusValue = $this->ModelObj['Author']->ModifyBlog( $_POST["Id"], $BlogFormData['Content'] );
                if ( ErrorCode::EverythingOkay == $StatusValue )
                {

                    header('Location:/Profile/Index?Message=Post has been modified successfully');
                }
                else
                {
                    header('Location: /BlogCrud/ModifyBlog?
                        Err='.$this->ModelObj['ErrorCode']->GetErrorDetail($StatusValue).'&
                        Id='.$_POST['Id']
                    );
                }
            }
            else
            {
                header('Location: /BlogCrud/ModifyBlog?Err='.$Status.'&Id='.$_POST['Id']);
            }
        }
        elseif('GET' == $_SERVER['REQUEST_METHOD'] && !empty($_GET['Id']))
        {
            if ( empty($this->ModelObj['Post']) )
            {
                $this->Model('Post');
            }
            $Data = $this->ModelObj['Post']->SearchPost(
                array('Id','Title', 'Content'),
                array('Id'),
                array('Id' => $_GET['Id']),
                array()
            );
            if ( is_array($Data) && count($Data) )
            {
                $Err = '';
                if ( !empty($_GET['Err']) )
                    $Err = $_GET['Err'];
                $this->View('Header',['IsLogInVar'=>1]);
                $this->View('Blog/Modify',['Data' => $Data,'Err' => $Err]);
            }
            else
            {
                header('Location:/Profile/Index?Err=Some error occurred Please try after sometime');
            }
        }
        else
        {
            header('Location: /Profile/Index?Err=Error occurred please try after sometime');
        }
    }

    public function Search()
    {
        $this->Model('Post');
        $this->Model('Utility');
        if ( 'GET' == $_SERVER['REQUEST_METHOD'] )
        {
            if ( empty($_GET['Month'] || empty($_GET['Year'])) )
            {
                header('Location:/Main/Index?Err=Some error occurred while searching. Please Try again later');
            }
            else
            {
                $this->Model('Post');
                $Result = $this->ModelObj['Post']->GetPostByMonth($_GET['Month'], $_GET['Year']);
                foreach ( $Result as $Row=>$Value )
                {
                    $Result[$Row]['AuthorName'] = $this->ModelObj['Utility']->GetAuthor($Value['UserId']);
                }
                $IsLogin = 0;
                if ( $this->ModelObj['Utility']->IsLogin(0) || $this->ModelObj['Utility']->IsLogin(1) )
                {
                    $IsLogin = 1;
                }
                $Message = '';
                $Err = '';
                if (!empty($_GET['Message']))
                {
                    $Message = $_GET['Message'];
                }
                if (!empty($_GET['Err']))
                {
                    $Err = $_GET['Err'];
                }
                $this->View('Header',['IsLogInVar' => $IsLogin]);
                $this->View('Blog/Search',['Result' => $Result,'Err'=>$Err,'Message'=>$Message]);
            }
        }
        else
        {
            header('Location:/Main/Index?Err=Some error occurred while searching. Please Try again later');
        }
    }

}