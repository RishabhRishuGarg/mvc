<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 21/2/19
 * Time: 12:12 PM
 */

/**
 *  Responsible for handling the error code and getting their descriptions
 *
 * @package utilities
 * */
class ErrorCode
{
    public const DatabaseConnectionError = 1001;
    public const DatabaseInsertionError  = 1002;
    public const DatabaseDeletionError   = 1003;


    public const WrongArgumentPassed     = 1101;
    public const WrongInformationEntered = 1102;
    public const NotLoggedIn             = 1103;
    public const InvalidEmail            = 1104;
    public const InvalidPassword         = 1105;
    public const InvalidName             = 1106;
    public const InavlidUser             = 1107;
    public const EmailExist              = 1108;
    public const PhoneNumberExist        = 1109;
    public const InvalidPost             = 1110;
    public const TitleExist              = 1111;

    public const EverythingOkay          = 2000;


    /**
     *  Map the error code with its description
     *
     * @param integer contains the error code
     *
     * @return string containing the description of Error code
     * */
    public function GetErrorDetail($Error)
    {
        switch ($Error)
        {
            case self::DatabaseConnectionError :
                return 'Database connection error occured';

            case self::EverythingOkay:
                return 'Everything worked fine';

            case self::WrongArgumentPassed:
                return 'Invalid information passed in function argument';

            case self::DatabaseInsertionError:
                return 'Database value insertion error occured';

            case self::WrongInformationEntered:
                return 'Wrong Information Entered';

            case self::InvalidEmail:
                return 'Invalid email passed';

            case self::InvalidPassword:
                return 'Invalid Password passed';

            case self::InvalidName:
                return 'Invalid Name entered';

            case self::InavlidUser:
                return 'Invalid User';

            case self::DatabaseDeletionError:
                return 'Database Deletion error occured';

            case self::EmailExist:
                return 'Email id Already exists';

            case self::PhoneNumberExist:
                return 'Phone Number already exists';

            case self::InvalidPost:
                return 'Post is Invalid';

            case self::TitleExist:
                return 'Title already exist';
            default :
                return 'Invalid code';
        }
    }

}