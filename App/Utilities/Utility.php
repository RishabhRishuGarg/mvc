<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 22/2/19
 * Time: 11:12 AM
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/DatabaseHandling.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/ErrorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Model/Author.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Model/Viewer.php';



/**
 * Class is providing some extra utilities relates to this project
 *
 * @package Utilities
 * */

class Utility
{
    private $DatabaseHandlingObj;

    /**
     * Utility constructor.
     *
     *  Constructor is responsible for starting the session and creating the instance of Database handler
     */
    function __construct()
    {
        if (session_status() == PHP_SESSION_NONE)
        {
            session_start();
        }
        $this->DatabaseHandlingObj = new DatabaseHandling();
    }

    /**
     * @param $UserId
     *
     * @return bool
     */
    public function IsAuthor($UserId)
    {
        $Data = $this->DatabaseHandlingObj->Select(
            UserTableName,
            array('IsAuthor'),
            array('Email'),
            array('Email' => $UserId),
            array()
        );
        return is_array($Data) && count($Data) && $Data[0]['IsAuthor'] ? true : false;
    }
    /**
     * Function is checking the details of current user who is logged in .
     * if not logged in then it will return error
     *
     * @return int|Author|Viewer
     */
    public function GetUserObject()
    {
        if ( empty($_SESSION['UserId']) )
        {
            return ErrorCode::NotLoggedIn;
        }
        else
        {
            $UserId = $_SESSION['UserId'];
            $Data = $this->DatabaseHandlingObj->Select(
              UserTableName,
              array('IsAuthor'),
              array('Email'),
              array('Email' => $UserId),
              array()
            );
            if ( is_array($Data) && count($Data) )
            {
                if ( $Data[0]['IsAuthor'] )
                {
                    return new Author();
                }
                else
                {
                    return new Viewer();
                }
            }
            else
            {
                return ErrorCode::WrongInformationEntered;
            }
        }
    }

    /**
     * Function is responsible of creating its class object and returning it
     *
     * @return Utility
     */
    public static function Factory()
    {
        return new Utility();
    }

    /**
     *  Function is checking if valid user is logged in or not
     *
     * @param $IsAuthor
     *
     * @return bool
     */
    public function IsLogin($IsAuthor)
    {
        if ( empty($_SESSION['UserId']) )
        {
            return false;
        }
        else
        {
            $Data = $this->DatabaseHandlingObj->Select(
                UserTableName,
                array('IsAuthor'),
                array('Email'),
                array('Email' => $_SESSION['UserId']),
                array()
            );
            if ( is_array($Data) && count( $Data ) && $Data[0]['IsAuthor'] == $IsAuthor)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }

    /**
     * Function checks if particular value exist in table or not
     *
     * @param $TableName
     * @param $Attributes
     * @param $Value
     *
     * @return bool
     */
    public function IsExist($TableName, $Attributes, $Value)
    {
        $Data = $this->DatabaseHandlingObj->Select(
            $TableName,
            array('Id'),
            $Attributes,
            $Value,
            array()
        );
        if ( is_array($Data) && count($Data) )
        {
            return true;
        }
        else
        {
            false;
        }
    }

    /**
     * Function return the name of author only
     *
     * @param $Id
     *
     * @return string
     */
    public function GetAuthor($Id)
    {
        $Data = $this->DatabaseHandlingObj->select(
            UserTableName,
            array('FirstName','LastName'),
            array('Id'),
            array('Id'=>$Id),
            array()
        );
        if( is_array($Data) && count($Data))
        {
            return $Data[0]['FirstName'].' '.$Data[0]['LastName'];
        }
        return '';
    }


    /**
     * Function is responsible of setting the anonymous in User table in database
     *
     * @return bool
     *
     */
    public function SetAnanymous()
    {
        $Data = $this->DatabaseHandlingObj->Select(
            UserTableName,
            array('Id'),
            array('FirstName'),
            array('FirstName' => Anonymous),
            array()
        );
        if ( is_array($Data) && count($Data) )
        {
            return true;
        }
        else
        {
            $Attribue =  array(
                'FirstName',
                'LastName',
                'Email',
                'Password',
                'NickName',
                'IsAuthor',
                'Address',
                'Country',
                'State',
                'City',
                'Pincode',
                'PhoneNumber'
            );
            $Value =  array(
                'FirstName'=>Anonymous,
                'LastName'=>'',
                'Email'=>'',
                'Password'=>'',
                'NickName'=>'',
                'IsAuthor'=>'',
                'Address'=>'',
                'Country'=>'',
                'State'=>'',
                'City'=>'',
                'Pincode'=>'',
                'PhoneNumber'=>''
            );
            $Status = $this->DatabaseHandlingObj->Insert(UserTableName, $Attribue, $Value);
            if ( ErrorCode::EverythingOkay ==$Status )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    /**
     *  Function get the data of profile of user
     *
     * @return string
     */
    public function GetDataForProfile()
    {
        $ProfileData = $this->GetUserObject()->Show();
        if (!empty($ProfileData['Posts']))
        {
            foreach ( $ProfileData['Posts'] as $Index => $PostData )
            {
                $ProfileData['Posts'][$Index]['AuthorName'] = $this->GetAuthor($PostData['UserId']);
            }
        }
        return $ProfileData;
    }
}