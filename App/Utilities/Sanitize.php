<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 22/2/19
 * Time: 1:07 PM
 */

class Sanitize
{
    public function Sanitization(&$FormData)
    {
        foreach ( $FormData as $Attribute => $Value )
        {
//            $FormData[$Attribute] = filter_var($Value, FILTER_SANITIZE_STRING);
            $FormData[$Attribute] = addslashes($FormData[$Attribute]);
            $FormData[$Attribute] = htmlentities($FormData[$Attribute],ENT_QUOTES);
        }
    }

    public function DeSanitize(&$FormData)
    {
        foreach ( $FormData as $Attribute => $Value )
        {
            $FormData[$Attribute] = stripslashes($FormData[$Attribute]);
            $FormData[$Attribute] = html_entity_decode($FormData[$Attribute],ENT_QUOTES);
        }
    }



}