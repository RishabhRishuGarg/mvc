<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 21/2/19
 * Time: 12:05 PM
 */

require_once $_SERVER['DOCUMENT_ROOT'] . "/core/config/Database.php";

/**
 *  handling all operation applied on database like insert update select
 *
 * @package databaseOperations
 *
 * */
class DatabaseHandling
{

    private $Conn;
    private $ErrorCodeObj;


    /**
     *  Create the connection to database set the Conn data memeber with database connection instance
     *
     * @return  integer Status of operation
     *
     * */
    public function GetConnection()
    {
        if (NULL == $this->Conn)
        {   $this->Conn = new PDO("mysql:host=".Servername.";dbname=".DBname,
            Username,
            Password
        );
            // set the PDO error mode to exception
            if ( NULL == $this->Conn )
            {
                return ErrorCode::DatabaseConnectionError;
            }

            $this->Conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return ErrorCode::EverythingOkay;
        }
        else
        {
            return ErrorCode::EverythingOkay;
        }
    }
    /**
     * DatabaseHandling constructor.
     *
     * @return void
     *
     */
    function __construct()
    {

        $this->ErrorCodeObj = new ErrorCode();
        $StatusCode = $this->GetConnection();
        $this->ErrorCodeObj->GetErrorDetail($StatusCode);

    }

    /**
     *  Function implements the insertion operation in database
     *
     * @param $TableName ,String table name on which insertion operation has to made
     * @param $Attributes array consist the attribute name to be store in database
     * @param $Values ,Associative array consist the values of all attributes to be store in database
     *
     * @return Integer which defines the status of operation
     *
     */
    public function Insert($TableName, $Attributes, $Values)
    {
        if ( ! count($Attributes) == count($Values) )
        {
            return errorCode::WrongArgumentPassed;
        }
        $Query = 'INSERT INTO '.$TableName.'(';
        for ( $i=0; $i<count($Attributes); $i++ )
        {
            if ( $i)
            {
                $Query = $Query.',';
            }
            $Query = $Query. $Attributes[$i];
        }
        $Query = $Query.') VALUES(';
        for ( $i=0; $i<count($Attributes); $i++ )
        {
            if ( $i)
            {
                $Query = $Query.',';
            }
            $Query = $Query.":".$Attributes[$i];
        }
        $Query = $Query.");";
        $Stmt = $this->Conn->prepare($Query);
        if ( $Stmt->execute($Values) )
        {
            return ErrorCode::EverythingOkay;
        }
        else
        {
            return ErrorCode::DatabaseInsertionError;
        }

    }


    /**
     *  Function implements the Modify operation on database
     *
     * @param $TableName , Table name on which operation has to made
     * @param $ColumnAttributes, attributes on which changes has to made
     * @param $ConditionAttributes, attributes on which condition has to check
     * @param $Values , values of all attributes as associative array
     * @param $Conditions , Condition operators which combines the multiple conditions
     *
     * @return Integer Status of operation
     */
    public function Modify(
        $TableName,
        $ColumnAttributes,
        $ConditionAttributes,
        $Values,
        $Conditions
    )
    {
        $Query = "UPDATE ".$TableName." SET ";
        for ( $i=0; $i<count($ColumnAttributes); $i++ )
        {
            if ( $i )
            {
                $Query = $Query." , ";
            }
            $Query = $Query.$ColumnAttributes[$i]."=:".$ColumnAttributes[$i];
        }
        $Query = $Query." WHERE ";

        for ( $i=0; $i < count($ConditionAttributes); $i++)
        {
            if ( $i )
            {
                $Query = $Query." ".$Conditions[$i]." ";
            }
            $Query = $Query.$ConditionAttributes[$i]."=:".$ConditionAttributes[$i];
        }
        $Query = $Query.";";

        $Stmt = $this->Conn->prepare($Query);
        if ( $Stmt->execute($Values) )
        {
            return ErrorCode::EverythingOkay;
        }
        else
        {
            return ErrorCode::DatabaseInsertionError;
        }
    }

    /**
     *  Function Implements the retrieve(Select) operation from database
     *
     * @param $TableName
     * @param $ColumnAttributes
     * @param $ConditionAttributes
     * @param $Values
     * @param $Conditions
     *
     * @param int $SelfString
     * @param string $String
     * @return int, array
     */
    public function Select(
        $TableName,
        $ColumnAttributes,
        $ConditionAttributes,
        $Values,
        $Conditions,
        $SelfString = 0,
        $String = ''
    )
    {
        $Query = "SELECT ";
        for ( $i = 0; $i < count($ColumnAttributes); $i++ )
        {
            if ( $i )
            {
                $Query = $Query." , ";
            }
            $Query = $Query.$ColumnAttributes[$i];
        }
        $Query = $Query." FROM ".$TableName;
        if ( 0 < count($ConditionAttributes))
        {
            $Query = $Query." WHERE ";
        }

        for ( $i=0; $i < count($ConditionAttributes); $i++)
        {
            if ( $i )
            {
                $Query = $Query." ".$Conditions[$i]." ";
            }
            $Query = $Query.$ConditionAttributes[$i]."=:".$ConditionAttributes[$i];
        }
        if ( $SelfString )
        {
            $Query = $Query.' '.$String;
        }
        $Query = $Query.';';
        $Stmt = $this->Conn->prepare($Query);
        if ( $Stmt->execute($Values) )
        {
            $Stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $Stmt->fetchAll();
        }
        else
        {
            return ErrorCode::DatabaseInsertionError;
        }

    }

    /**
     *  Function implements the Delete from table and execute the query
     *
     * @param $TableName
     * @param $ConditionAttributes
     * @param $Values
     * @param $Conditions
     * @return int
     */
    public function Delete(
        $TableName,
        $ConditionAttributes,
        $Values,
        $Conditions
    )
    {
        $Query = 'DELETE FROM '.$TableName.' WHERE ';
        for ( $i=0; $i < count($ConditionAttributes); $i++)
        {
            if ( $i )
            {
                $Query = $Query." ".$Conditions[$i]." ";
            }
            $Query = $Query.$ConditionAttributes[$i]."=:".$ConditionAttributes[$i];
        }
        $Query = $Query.";";

        $Stmt = $this->Conn->prepare($Query);
        if ( $Stmt->execute($Values) )
        {
            return ErrorCode::EverythingOkay;
        }
        else
        {
            return ErrorCode::DatabaseDeletionError;
        }
    }


}