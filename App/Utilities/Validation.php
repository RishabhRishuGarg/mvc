<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 22/2/19
 * Time: 11:31 AM
 */

/**
 * Class is responsible for functionality related to validation of data
 *
 * @package Utilities
 *
 */
class Validation
{

    /**
     * Function is checking for attributes are empty or not
     *
     * @param $Required
     *
     * @return string|integer
     */

    public function IsEmpty($Required)
    {
        foreach ( $Required as $Value )
        {
            if ( empty( $_POST[$Value ] ) )
            {
                return $Value.' Is Required';
            }
        }
        return ErrorCode::EverythingOkay;
    }
   /**
     * Function is responsible of creating its class object and returning it
     *
     * @return Validation
     */
    public static function Factory()
    {
        return new Validation();
    }

    /**
     * Function is checking if email is valid or not
     *
     * @param $Email
     *
     * @return int
     */
    public function EmailCheck($Email )
    {
        if ( filter_var($_POST["Email"], FILTER_VALIDATE_EMAIL) )
        {
            return ErrorCode::EverythingOkay;
        }
        else
        {
            return ErrorCode::InvalidEmail;
        }
    }

    /**
     * Function is checking if Password is valid or not
     *
     * @param $Password
     *
     * @return int
     */
    public function PasswordCheck($Password )
    {
        return preg_match('(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}', $Password) ?
            ErrorCode::InvalidPassword : ErrorCode::EverythingOkay;
    }

    /**
     * Function is checking if Name is valid or not
     *
     * @param $Name
     *
     * @return int
     */
    public function NameCheck($Name )
    {
        return preg_match('/^[a-zA-Z ]*$/', $Name) ? true : false;
    }
}