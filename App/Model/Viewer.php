<?php

/**
 *  This class is related to all functionality on Posts.
 *
 * @package Core
 * */
class Viewer extends User implements Display 
{



    /**
     * This function return that what data to be display over the screen.
     *
     *
     * @return int|array
     * */
    public function Show()
    {

        $DisplayData['Details'] = $this->DatabaseOperation->Select(
            UserTableName,
            array(
                'Id',
                'FirstName',
                'LastName',
                'Email',
                'PhoneNumber',
                'Address',
                'State',
                'City',
                'Country',
                'NickName',
                'Pincode',
                'IsAuthor'
            ),
            array('Email'),
            array('Email' => $this->UserId),
            array()
        );

        if (is_array($DisplayData['Details']) && count($DisplayData['Details'])) {
            return $DisplayData;
        }
        else
        {
            return ErrorCode::WrongInformationEntered;
        }

    }

}