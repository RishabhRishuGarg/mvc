<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 25/2/19
 * Time: 12:57 PM
 */


require_once $_SERVER['DOCUMENT_ROOT'].'/App/Model/User.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/App/Utilities/ErrorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/App/Utilities/Utility.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/App/Utilities/DatabaseHandling.php';



/**
 * This class handles all functionalists related to comment like add comment, delete comment
 *
 * @package Core
 * */
class Comment
{
    private $UserObj;
    private $PostObj;
    private $ErrorObj;
    private $UtilityObj;
    private $DatabaseHandlingObj;



    /**
     * Initialize all the object varibles for further use
     *
     * Comment constructor.
     */
    function __construct()
    {
        $this->UserObj             = new User();
        $this->PostObj             = new Post();
        $this->UtilityObj          = new Utility();
        $this->ErrorObj            = new ErrorCode();
        $this->DatabaseHandlingObj = new DatabaseHandling();
    }

    /**
     * Function handles the add new comment functionalists for a Post
     *
     * @param $UserId
     * @param $PostId
     * @param $CommentData
     *
     * @return int
     */
    public function AddComment($PostId, $CommentData )
    {

        $UserId = '';
        if ( $this->UtilityObj->IsLogin(1) || $this->UtilityObj->IsLogin(0) )
        {
            $UserId = $_SESSION['UserId'];
        }
        else
        {
            $Data = $this->DatabaseHandlingObj->Select(
                CommentTableName,
                array('Id'),
                array('FirstName'),
                array('FirstName'=>Anonymous),
                array()
            );
            $UserId = $Data[0]['Id'];
        }
        if ( !strlen($CommentData) )
        {
            return ErrorCode::WrongArgumentPassed;
        }
        if ( $this->UtilityObj->IsExist(PostTableName, array('Id'), array('Id' => $PostId) ) )
        {
            return $this->DatabaseHandlingObj->Insert(
                CommentTableName,
                array('UserId', 'PostId', 'Value'),
                array(
                    'UserId' => $UserId,
                    'PostId' => $PostId,
                    'Value'  => $CommentData
                )
            );
        }
        else
        {
            return ErrorCode::InvalidPost;
        }
    }

    /**
     * Function is handling the request to getting the comments for a particular Post
     *
     * @param $PostId
     *
     * @return int
     */
    public function GetComment($PostId)
    {
        return $this->DatabaseHandlingObj->Select(
            CommentTableName,
            array('UserId', 'Value', 'Added'),
            array('PostId'),
            array('PostId' => $PostId),
            array()
        );
    }
}