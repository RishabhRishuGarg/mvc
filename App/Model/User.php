<?php

/**
 * 
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/ErrorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Model/Post.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/DatabaseHandling.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/Utility.php';



/**
 * Class is responsible of all functionalists that user can demand for.
 *
 * @package Core
 *
 * */
class User
{
    protected $DatabaseOperation;
    protected $PostObj;
    protected $UserId = "";
    protected $UtilityObj;

    /**
     *
     * User constructor.
     *
     * Starting the session and setting up the UserId to current logged in user.
     *
     */
    function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) 
        {
            session_start();
        }
        if (isset($_SESSION['UserId']))
        {
            $this->UserId = $_SESSION["UserId"];
        }
        else
        {
            $_SESSION["UserId"] = "";
        }
        $this->PostObj           = new Post();
        $this->DatabaseOperation = new DatabaseHandling();
        $this->UtilityObj = new Utility();
    }


    /**
     * Function is responsible of creating its class object and returning it
     *
     * @return User
     *
     */
    public static function Factory()
    {
            return new User();
    }



    /**
     * Function is Handling the Search operation on what conditions they need the results
     *
     * @param $Attributes
     * @param $TableName
     *
     *
     * @return array|int
     */
    public function Search($Attributes, $TableName)
    {   
        //$Results = $this->DatabaseOperation->Select($Attributes, $TableName);
        if (empty($Results))
        {
            return 0;
        }
        else
        {
            return $Results;
        } 
    }



    /**
     *
     *  Funtion is handling the login feature
     *
     * @param $UserId
     * @param $Password
     * @param $IsAuthor
     *
     * @return int
     */
    public function Login($UserId, $Password, $IsAuthor)
    {
        echo $UserId;
        echo $Password;
        echo $IsAuthor;
        $Value = $this->DatabaseOperation->Select(
            UserTableName,
            array('Password','IsAuthor'),
            array('Email'),
            array('Email' => $UserId),
            array()
        );
        if (
            is_array($Value)                   &&
            count($Value)                      &&
            $IsAuthor == $Value[0]['IsAuthor'] &&
            password_verify($Password, $Value[0]['Password'])
        )
        {
                $_SESSION['UserId'] = $UserId;
                return ErrorCode::EverythingOkay;
        }
        else
        {
            return ErrorCode::WrongInformationEntered;
        }

    }


    /**
     * Function implements the sign up functionality
     *
     * @param $FirstName
     * @param $LastName
     * @param $Email
     * @param $Password
     * @param $NickName
     * @param $IsAuthor
     * @param $Address
     * @param $Country
     * @param $State
     * @param $City
     * @param $Pincode
     * @param $PhoneNumber
     *
     * @return int
     */
    public function Signup(
        $FirstName,
        $LastName,
        $Email,
        $Password='',
        $NickName='',
        $IsAuthor=0,
        $Address='',
        $Country='',
        $State='',
        $City='',
        $Pincode='',
        $PhoneNumber=''
    )

    {
        if ( $this->UtilityObj->IsExist(UserTableName, array('Email'), array('Email'=>$Email)) )
        {
            return ErrorCode::EmailExist;
        }
        $Attribue =  array(
            'FirstName',
            'LastName',
            'Email',
            'Password',
            'NickName',
            'IsAuthor',
            'Address',
            'Country',
            'State',
            'City',
            'Pincode',
            'PhoneNumber'
        );
        $Value =  array(
            'FirstName'=>$FirstName,
            'LastName'=>$LastName,
            'Email'=>$Email,
            'Password'=>$Password,
            'NickName'=>$NickName,
            'IsAuthor'=>$IsAuthor,
            'Address'=>$Address,
            'Country'=>$Country,
            'State'=>$State,
            'City'=>$City,
            'Pincode'=>$Pincode,
            'PhoneNumber'=>$PhoneNumber
        );
        $Status =  $this->DatabaseOperation->Insert(UserTableName, $Attribue, $Value);
        return $Status;
    }

    public function AddVote( $PostId, $VoteType )
    {

        if ( empty($this->UserId) )
        {
            $Data = $this->DatabaseOperation->select(
                UserTableName,
                array('Id'),
                array('FirstName'),
                array('FirstName' => Anonymous),
                array()
            );
            if ( is_array($Data) && count($Data) )
            {
                $this->PostObj->AddVote($Data[0]['Id'], $PostId, $VoteType);
            }
        }
        else
        {
            $this->PostObj->AddVote($this->UserId, $PostId, $VoteType);
        }
    }
}




