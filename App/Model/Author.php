<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Model/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/Utility.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Model/GetSetInterface.php';


/**
 * Author class implements the functions that a author can do.
 *
 * @package Core
 * */
class Author extends User implements Display,BasicOperations
{

    /**
     * This  function is responsible of submitting the request to get the Author data from database
     *
     * @return mixed
     */
    public function Show()
    {
        $DisplayData['Posts'] = $this->DatabaseOperation->Select(
            PostTableName,
            array('Id','UserId' , 'Title', 'Content', 'Modified'),
            array(),
            array(),
            array(),
            1,
            'WHERE UserId IN ( SELECT Id FROM User WHERE Email="'. $this->UserId .'" )'

        );
        if (  is_array($DisplayData['Posts']) ) {

            $DisplayData['Details'] = $this->DatabaseOperation->Select(
                UserTableName,
                array(
                    'Id',
                    'FirstName',
                    'LastName',
                    'Email',
                    'PhoneNumber',
                    'Address',
                    'State',
                    'City',
                    'Country',
                    'NickName',
                    'Pincode',
                    'IsAuthor'
                ),
                array('Email'),
                array('Email' => $this->UserId),
                array()
            );
            if (is_array($DisplayData['Details']) && count($DisplayData['Details'])) {
                return $DisplayData;
            }
            else
            {
                return ErrorCode::WrongInformationEntered;
            }
        }
        else
        {
            return ErrorCode::WrongInformationEntered;
        }
    }





    /**
     * This function take id and Title as input and store the info in Database
     *
     * @param $BlogData
     *
     * @return int
     */
    public function Write($BlogData)
    {
        $UtilityObj = new Utility();
        if ( $UtilityObj->IsExist(PostTableName, array('Title'), array('Title' => $BlogData['Title']) ) )
        {
            return ErrorCode::TitleExist;
        }
        return $this->PostObj->AddPost(
                $this->UserId,
                $BlogData['Title'],
                $BlogData['Content']
            );
    }

    /**
     * Function implements the functionality of modification of blog
     *
     * @param $PostId
     * @param $Content
     *
     * @return int
     */
    public function ModifyBlog($PostId, $Content)
    {
        $Status = $this->PostObj->SearchPost(
            array('UserId'),
            array('Id'),
            array('Id' => $PostId),
            array()
        );
        $UserId = $this->DatabaseOperation->Select(
            UserTableName,
            array('Id'),
            array('Email'),
            array('Email'=>$this->UserId),
            array()
        );
        if ( is_array($UserId) && count($UserId) )
        {
            $UserId = $UserId[0]['Id'];
        }
        else
        {
            return ErrorCode::InavlidUser;
        }
        if ( is_array($Status) && count($Status) && $Status[0]['UserId'] == $UserId )
        {
            $Status = $this->PostObj->ModifyPost( $PostId, $Content );
        }
        else
        {
            return ErrorCode::InavlidUser;
        }
        return $Status;

    }


    /**
     *  Function check if user is authorized to delete post or not
     *  if yes send the request to post class to delete the post
     *
     * @param $Id
     *
     * @return int
     */
    public function RemovePost($Id)
    {
        if ( is_numeric( $Id ) )
        {
            if ($this->UtilityObj->IsAuthor($this->UserId))
            {
                return $this->PostObj->DeletePost( $Id );
            }
            else
            {
                return ErrorCode::InavlidUser;
            }
        }
        else
        {
            ErrorCode::WrongArgumentPassed;
        }
    }
}