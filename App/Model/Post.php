<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 21/2/19
 * Time: 1:50 PM
 */


require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/DatabaseHandling.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/Utility.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/App/Utilities/ErrorCode.php';


/**
 *
 * Class implements all functionality of post like add post ,delete post, Modify post
 *
 * @package Core
 *
 * */
class Post
{

    private $DatabaseObj ;
    private $UtilityObj;


    /**
     *  This costructor initializing the database object with instance of Database handling class
     *
     * Post constructor.
     */
    function __construct()
    {
        $this->DatabaseObj = new DatabaseHandling();
        $this->UtilityObj  = new Utility();

    }

    /**
     *  Function implements the Add post functionality it send the request to add new post into database
     *
     * @param $UserId
     * @param $Title
     * @param $Content
     *
     * @return int
     */
    public function AddPost($UserId, $Title, $Content)
    {

        if ( !$this->UtilityObj->IsAuthor($UserId))
        {
            return ErrorCode::InavlidUser;
        }
        $Status = $this->DatabaseObj->Insert(
            PostTableName,
            array('UserId','Title','Content'),
            array(
                'UserId' => $UserId,
                'Title'  => $Title,
                'Content'=> $Content
            )
        );
        return $Status;
    }

    /**
     * Function implements the Delete post functionality it send the request to delete post into database
     *
     * @param $Id
     *
     * @return int
     */
    public function DeletePost( $Id )
    {
        return $this->DatabaseObj->Delete(
            PostTableName,
            array('Id'),
            array('Id' => $Id),
            array()
        );
    }

    /**
     * Function implements the modify post functionality, Generates the request to modify post to
     * database handling.
     *
     * @param $Id
     * @param $Content
     *
     * @return int
     */
    public function ModifyPost(
        $Id,
        $Content
    )
    {
        return $this->DatabaseObj->Modify(
            PostTableName,
            array('Content'),
            array('Id'),
            array('Content' => $Content, 'Id' => $Id),
            array()
        );

    }

    /**
     *  Function gets the latest blogs according to modified date
     *
     * @return int|array
     */
    public function GetLatestPosts()
    {
        $Data = $this->DatabaseObj->Select(
            PostTableName,
            array('Id', 'UserId' , 'Title', 'Content', 'Modified'),
            array(),
            array(),
            array(),
            1,
            ' ORDER BY Modified DESC'
        );
        return $Data;
    }

    public function GetPostByMonth( $Month, $Year )
    {
        if ( 1 == strlen($Month) )
        {
            $Month = '0'.$Month;
        }
        $Data = $this->DatabaseObj->select(
            PostTableName,
            array('Id', 'UserId' , 'Title', 'Content', 'Modified'),
            array(),
            array(),
            array(),
            1,
            ' WHERE Modified LIKE "'.$Year.'-'.$Month.'%"'
        );
        return $Data;
    }

    /**
     * @param $Attributes
     * @param $ConditionsAttributes
     * @param $Values
     * @param $Conditions
     *
     * @return int
     */
    public function SearchPost($Attributes, $ConditionsAttributes, $Values, $Conditions )
    {
        return $this->DatabaseObj->Select(
            PostTableName,
            $Attributes,
            $ConditionsAttributes,
            $Values,
            $Conditions
        );
    }
    public function AddVote( $UserId, $PostId, $VoteType)
    {
        if ( $this->UtilityObj->IsExist(PostTableName, array('Id'), array('Id' => $PostId) ) )
        {
            if ( $this->UtilityObj->IsExist(UserTableName, array('Id'), array('Id' => $UserId) ) )
            {
                $this->DatabaseObj->IncreaseVote($UserId, $PostId, $VoteType);
            }
            else
            {
                return ErrorCode::InavlidUser;
            }
        }
        else
        {
            return ErrorCode::InvalidPost;
        }
    }
}