  <meta charset = "utf-8">
    <meta http-equiv = "X-UA-Compatible" content = "IE = edge">
    <meta name = "viewport" content = "width = device-width, initial-scale = 1">
    <title> Blogging </title>

    <div class = "container Navigation">
        <nav class="navbar navbar-inverse navbar-fixed-top  ">
            <div class="container-fluid">        
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">Blogging</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Home</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                <?php


                    if ( !empty($Data['IsLogInVar']) && $Data['IsLogInVar'] )
                    {
                        echo
                            "<li><a href='/Profile'><span class='glyphicon glyphicon-user'></span> Profile</a></li>"
                            ."<li><a href='/Authentication/Logout'><span class='glyphicon glyphicon-log-out'></span> Logout</a></li>";
                    }
                    else
                    {
                        echo
                            "<li><a href='/Authentication/Signup'><span class='glyphicon glyphicon-user'></span> Sign Up</a></li>"
                            ."<li><a href='/Authentication/Login'><span class='glyphicon glyphicon-log-in'></span> Login</a></li>";
                    }
                ?>
                </ul>
            </div>
        </nav>

    </div>
        


    