<!DOCTYPE html>
<html>
<head>
    <title>Profile</title>
</head>
<body >
    <div class="container BottomNav">
        <?php

            if(!empty($Data['Err']))
            {
                echo "<h3 style='color:Red;'> ".$_GET['Err']. "</h3>";
            }
            if(!empty($Data['Message']))
            {
                echo "<h3 style='color:Green;'> ".$_GET['Message']. "</h3>";
            }
            extract($Data);
        ?>
        <div class="row">
            <div class = " col-sm-offset-2 col-sm-8">
                <div class="Profile">
                  <table class = "Profile">
                    <tbody>
                        <tr>
                            <th scope="row">*</th>
                            <td><strong>Name</strong></td>
                            <td><?php echo $Data['Details'][0]["FirstName"]." ".$Data["Details"][0]["LastName"];?></td>
                        </tr>
                        <tr>
                            <th scope="row">*</th>
                            <td><strong>Email</strong></td>
                            <td><?php echo $Data["Details"][0]["Email"];?></td>
                        </tr>
                        <tr>
                            <th scope="row">*</th>
                            <td><strong>Nick Name</strong></td>
                            <td><?php echo $Data["Details"][0]["NickName"];?></td>
                        </tr>
                        <tr>
                            <th scope="row">*</th>
                            <td><strong>Address</strong></td>
                            <td>
                                <?php

                                    if(!empty($Data['Details']['Address']))
                                    echo $Data['Details'][0]['Address'].
                                    ", ".$Data['Details'][0]['City']." (".$Data['Details'][0]['Pincode'].
                                    "), ".$Data['Details'][0]['State'].
                                    ", ".$Data['Details'][0]['Country'] ;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">*</th>
                            <td><strong>Phone Number</strong></td>
                            <td><?php echo $Data["Details"][0]["PhoneNumber"];?></td>
                        </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
        <div class="container">
        <?php
            if (1 == ($Data["Details"][0]["IsAuthor"]))
            {

                echo " <div class='row AddPostButton' > <button class='btn-info' onclick='AddBlog()'> Add Blog </button></div>";
                if (!empty($Data['Posts']))
                {
                    echo "<h3>Blogs published by you</h3>";
                }
                echo "<div class='row'>";
                foreach ($Data["Posts"] as $Artical => $ArticalData)
                {

                    echo "
                    <div class='row'>     
                        <div class='col-sm-4'>
                            <h3 class='card-title'>".$ArticalData['Title']."</h3>
    
                            <div class='card' style='width:400px'>
                            
                                <div class='card-body'>
                                    <p class='card-text' style='padding:20px;'>".substr($ArticalData['Content'],0,200)."</p>
                                    <a href='/View/index.php?Id=". $ArticalData["Id"] ."' class='btn btn-primary'>Full Artical</a>
    
                                </div>
                                <div class='card-footer text-muted'>
                                      Posted on ".explode(" ",$ArticalData['Modified'])[0]." by
                                      ".$ArticalData['AuthorName'] ."
                                </div>
                            </div>
                        </div>
                        
                        <div class='col-sm-2'>
                            <a href='/BlogCrud/DeleteBlog?Id=".$ArticalData["Id"]."'> 
                                <img src='/Public/Images/Profile/BlogDelete.png' alt='Delete Post' height='20' width='20'>
                            </a>
                            <a href='/BlogCrud/ModifyBlog?Id=".$ArticalData["Id"]."'> 
                                <img src='/Public/Images/Profile/Modify.jpeg' alt='Modify Post' height='20' width='20'>
                            </a>
                        </div>
                     </div>   
                        ";
                }
                echo "</div>";
          }
        ?>
        </div>
    </div>
</body>
</html>