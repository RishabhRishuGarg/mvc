<html lang="en">
<head>
    <title>
        Login
    </title>
</head>
<body>
<div class = "BottomNav Login">
    <form action = '/Authentication/Login' method = 'POST' >
        <?php
        if (!empty($Data['Err']))
            echo
                "<div class='alert alert-danger fade in' style='display: inline-block;'>" .
                "<strong>Error!</strong>".
                $Data['Err'] .
                "</div>";
        if (!empty($Data['Message']))
            echo
                "<div class='alert alert-info fade in' style='display: inline-block;'>" .
                "<strong>Message!</strong>".
                $Data['Message'] .
                "</div>";
        ?>

        <div class = "form-group">

            <label for  = "usr">UserId:</label>
            <input type = "text" class = "form-control" id = "usr" name = "UserId" style = "width: 20%;">

        </div>
        <div class = "form-group">
            <label for  = "Err">Password:</label>
            <input type = "password" class = "form-control" id = "Err" name = "Password" style = "width: 20%;">
        </div>
        <br>
        <div class = "form-check">
            <input class = "form-check-input" type = "checkbox" value = "1" name = "IsAuthor">
            <label class = "form-check-label" for = "defaultCheck1">
                IsAuthor
            </label>
        </div>

        <div>
            <input  class = "btn btn-info" type = "submit" value = "Submit" align = "right" >
            <a href=<?php echo $Data['LogInURL'];  ?> ><input class = "btn btn-danger" value="Log In with Google"></a>
        </div>

    </form>
</div>
</body>
</html>