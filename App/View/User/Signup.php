<!DOCTYPE html>
<html lang = "en">
    <head>
        <title> Blogging - SignUp </title>
    </head>
  <body >
    <div class = "BottomNav Signup" >

        <form action   = "/Authentication/SignUp" method = "POST" name="Form" onsubmit="return ValidateForm();">
            <?php

            if (!empty($Data['Err']))
                echo
                    "<div class='alert alert-danger fade in' style='display: inline-block;'>" .
                    "<strong>Error!</strong>".
                    $Data['Err'] .
                    "</div>";
            if (!empty($Data['Message']))
                echo
                    "<div class='alert alert-info fade in' style='display: inline-block;'>" .
                    "<strong>Message!</strong>".
                    $Data['Message'] .
                    "</div>";
            ?>
            <h4 color="red"> * marked fields are required to fill </h4>

            <div class="row">
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "usr">First Name <font color="red">*</font> :</label>
                        <input type = "text" class = "form-control" data-toggle="tooltip" title="Only characters and spaces are allowed" name = "FirstName" id = "FirstName"  maxlength="50" required>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "usr">Last Name <font color="red">*</font> :</label>
                        <input type = "text" class = "form-control" data-toggle="tooltip" title="Only characters and spaces are allowed" name = "LastName"  id = "LastName"  maxlength="50" >
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "Nick Name">Nick Name :</label>
                        <input type = "text" class = "form-control" data-toggle="tooltip" title="Only characters and spaces are allowed" name = "NickName" id = "NickName"  maxlength="100" >
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "email">Email <font color="red">*</font>:</label>
                        <input type = "email" class = "form-control" name = "Email" id = "Email"  maxlength="255" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "usr">Address <font color="red">*</font>:</label>
                        <input type = "text" class = "form-control" name = "Address" id = "Address"   maxlength="1000" required>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "usr">Country <font color="red">*</font>:</label>
                        <input type = "text" class = "form-control" data-toggle="tooltip" title="Only characters and spaces are allowed" name = "Country" id = "Country"  maxlength="20" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "usr">State <font color="red">*</font>:</label>
                        <input type = "text" class = "form-control" data-toggle="tooltip" title="Only characters and spaces are allowed" name = "State" id = "State"  maxlength="20" required>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "usr">City <font color="red">*</font>:</label>
                        <input type = "text" class = "form-control" data-toggle="tooltip" title="Only characters and spaces are allowed" name = "City" id = "City"  maxlength="20" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "usr">Pincode <font color="red">*</font>:</label>
                        <input type = "text" class = "form-control" data-toggle="tooltip" title="Only characters and spaces are allowed" name = "Pincode" id = "Pincode"  maxlength="6" required>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "usr">Phone Number <font color="red">*</font>:</label>
                        <input type = "text" class = "form-control" data-toggle="tooltip" title="Enter an valid Number" name = "PhoneNumber" id = "PhoneNumber"  maxlength="10" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "pwd">Password <font color="red">*</font>:</label>
                        <input type = "password" class = "form-control" name = "Password" id = "Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" maxlength="16"required>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class = "form-group">
                        <label for  = "pwd">Re-Enter Password <font color="red">*</font>:</label>
                        <input type = "password" class = "form-control"  name = "RePassword" id = "RePassword"  required>
                    </div>
                </div>
            </div>

            <div class = "form-check">
                <input class = "form-check-input" type = "checkbox" name = "IsAuthor" id = "IsAuthor" value="1">
                <label class = "form-check-label" for = "defaultCheck1">
                    IsAuthor
                </label>
            </div>

            <div>
                <input  class = "btn btn-info" type = "submit" value = "Submit" align = "right" >
            </div>
        </form>


    </div>
  </body>
 </html>