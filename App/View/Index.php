<!DOCTYPE html>
<html lang='en'>
<head>
    <title> Blogging - Home </title>
</head>
<body>

<div class="col-sm-9 BottomNav">
    <?php
    if (!empty($Data['Err']))
        echo
            "<div class='alert alert-danger fade in' style='display: inline-block;'>" .
            "<strong>Error!</strong>".
            $Data['Err'] .
            "</div>";
    if (!empty($Data['Message']))
        echo
            "<div class='alert alert-info fade in' style='display: inline-block;'>" .
            "<strong>Message!</strong>".
            $Data['Message'] .
            "</div>";


    ?>
    <div class='container'>

        <button type='button' class='btn btn-info' style='margin:3px;'>
            <a href='/' style="color: black"> Latest </a>
        </button>
        <div class='row'>
            <div class='card-deck'>
                <?php

                extract($Data);
                if (count($Result) == 0)
                {
                    echo "<h2> Oops no blog found!!!!!</h2>";
                }
                else
                {
                    foreach ($Result as $Row => $Data)
                    {
                        $time = strtotime($Data['Modified']);
                        $FormattedDateTime = date('m M/o g:i A', $time);

                        echo
                            "<div class='card col-sm-3' style='border: solid #D1CBE1 1px ; margin: 12px;'>".
                                "<div class='card-body'>".
                                    "<h3 class='card-title'>".
                                         $Data["Title"] .
                                    "</h3>".
                                    "<p class='card-text' style='padding:20px;'>".
                                        substr($Data['Content'], 0, 200) .
                                    "</p>".
                                    "<button type='button' class='btn btn-primary' style='margin:3px; '>".
                                        "<a href='/ViewBlog/Index?Id=" . $Data['Id'] . "' style='color:white;'> Read More </a>".
                                    "</button>".
                                    "<p class='card-text'>".
                                        "<small class='text-muted'>".
                                            "Last updated on " . $FormattedDateTime .
                                        "</small>".
                                    "</p>".
                                    "<p class='card-text' style ='color:red;'>".
                                        "<small class='text-muted'>".
                                            "Published By " . $Data['AuthorName'] .
                                        "</small>".
                                    "</p>".
                                "</div>".
                            "</div>";
                    }
                }
                ?>

            </div>
        </div>
    </div>
</div>

<div class="col-sm-2 Archive">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Archives
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="overflow: scroll; max-height: 1000%; min-height: 0%;">
            <?php

            $Year = date("Y");;
            $Month = date("m");
            $Count = 12;
            while ($Count > 0) {
                $dateObj = DateTime::createFromFormat('!m', $Month);
                $FormatMonth = $dateObj->format('F');
                echo "
                    <li class='list-group-item'>
                        <a href='/BlogCrud/Search?Month=" . $Month . "&Year=" . $Year . "'> " . $FormatMonth . " " . $Year . " </a>
                    </li>
                ";
                $Month--;
                if ($Month < 1) {
                    $Year--;
                    $Month = 12;
                }
                $Count--;
            }
            ?>
        </ul>
    </div>


</div>



</body>
</html>