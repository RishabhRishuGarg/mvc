<!--
/*
 * Created by PhpStorm.
 * User: mindfire
 * Date: 22/2/19
 * Time: 12:39 PM
 */-->
<!DOCTYPE html>
<html lang="en">
<head>
    <title> Add new Blog </title>

</head>
<body>
    <div class="container">
        <div class="BottomNav">
            <form action   = '/BlogCrud/AddBlog' method = 'POST' >
               <?php
               if (!empty($Data['Err']))
                   echo
                       "<div class='alert alert-danger fade in' style='display: inline-block;'>" .
                       "<strong>Error!</strong>".
                       $Data['Err'] .
                       "</div>";
               if (!empty($Data['Message']))
                   echo
                       "<div class='alert alert-info fade in' style='display: inline-block;'>" .
                       "<strong>Message!</strong>".
                       $Data['Message'] .
                       "</div>";
                ?>
                <div class = "form-group" >
                    <label for  = "Blog Title">Blog Title:</label>
                    <input type = "text" class = 'form-control' name = 'Title' value = <?php echo $Data['Title']?> placeholder="Enter title of blog">
                </div>
                <div class="form-group">
                </div>


                <div class = "form-group">
                    <label for  = "Content">Content:</label>
                    <textarea class = "form-control" name = "Content" rows="20" cols="70"><?php echo $Data['Content']?></textarea>

                </div>

                <div>
                    <input  class = "btn btn-info" type = "submit" value = "Add Blog" align = "right" >
                </div>

            </form>
        </div>
    </div>

</body>
</html>