<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 25/2/19
 * Time: 2:18 PM
 */
?>

<html lang="en">

    <head>
        <title> Blogging </title>
    </head>
    <body>
        <div class='container'>
            <div class='BottomNav'>
                <?php
                    if (!empty($Data['Err']))
                        echo
                            "<div class='alert alert-danger fade in' style='display: inline-block;'>" .
                                "<strong>Error!</strong>".
                                 $Data['Err'] .
                            "</div>";
                    if (!empty($Data['Message']))
                        echo
                            "<div class='alert alert-info fade in' style='display: inline-block;'>" .
                                "<strong>Message!</strong>".
                                $Data['Message'] .
                            "</div>";

                    extract($Data);
                    echo "<div class='row'>";
                    echo "<h1>" . $PostData['Title'] . "</h1>";
                    echo "</div>";

                    echo "<div class='row'>";
                    echo "<p style = 'border:solid 2px #427df4; padding:20px; ' >" . $PostData['Content'] . "</p>";
                    $time = strtotime($PostData['Modified']);
                    $FormattedDateTime = date('m M/o g:i A', $time);
                    echo "</div>";

                    echo "<div class='row'>";
                    echo "Last Modified on " . $FormattedDateTime . " By " . $PostData['AuthorName'];
                    echo "</div>";

                ?>
            </div>

            <div class="row">
                <div class="btn-group">
                    <button type='button' class='btn btn-sm Vote' onclick=""> UpVote </button>
                    <button type='button' class='btn btn-sm Vote' onclick=""> DownVote </button>
                </div>
            </div>
            <div class="row">
                <h3> Comments </h3>
            </div>
            <?php
                foreach ( $Comment as $Row => $Value )
                {
                    $time = strtotime($Value['Added']);
                    $FormattedDateTime = date('m M/o g:i A', $time);
                    echo
                        "<div class='Comment'>".
                            "<div class='Comment'". $Row ." onmouseover='return ShowComment(" . $Row . ")' >".
                                "<div class='row '>".
                                    "<div class='panel-group'>".
                                        "<div class='panel panel-default'>".
                                            "<div class='panel-heading'>".
                                                "<h4 class='panel-title'>".
                                                    "<a data-toggle='collapse' >".
                                                         $Value['UserName'].
                                                        "<p align='right'>".
                                                            $FormattedDateTime.
                                                        "</p>".
                                                    "</a>".
                                                "</h4>".
                                            "</div>".
                                        "<div id='collapse" . $Row . "' class='panel-collapse collapse'>".
                                            "<div class='panel-body'>" . $Value['Value'] . " </div>".
                                            "<div class='panel-footer'>".
                                                "<div align='right'>".
                                                    "<button type='button'  class='btn btn-info' onclick='return HideComment(" . $Row . ")'>".
                                                        "Close".
                                                    "</button>".
                                                "<div>".
                                            "</div>".
                                        "</div>".
                                    "</div>".
                                "</div>".
                            "</div>".
                        "</div>".
                    "</div>";
                }
            ?>


        <script>
            function ShowComment(id)
            {
                $('#collapse'+id).collapse('show');
            }
            function HideComment(id)
            {
                $('#collapse'+id).collapse('hide');
            }

        </script>
    </body>
</html>
