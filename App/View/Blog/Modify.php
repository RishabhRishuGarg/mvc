<!DOCTYPE html>
<html lang="en">
<head>
    <title> Modify Blog</title>
</head>
<body>
<div class="container">
    <div class="row BottomNav">
        <form action   = '/BlogCrud/ModifyBlog' method = 'POST' >
            <input class='form-inline' id='custId' name='Id' type='hidden' value=<?php echo $Data['Data'][0]['Id']; ?>
            <?php

            if (!empty($Data['Err']))
                echo "<div class='alert alert-danger fade in' style='display: inline-block;'>" .
                    "<strong>Error!</strong>"
                    . $Data['Err'] .
                    "</div>";
            if (!empty($Data['Message']))
                echo "<div class='alert alert-info fade in' style='display: inline-block;'>" .
                    "<strong>Message!</strong>"
                    .$Data['Message'] .
                    "</div>";
            extract($Data);
            ?>

            </div>
            <div class = "form-group">
                <label for  = 'Blog Title'> Blog Title: </label>
                <?php echo( "<input type = 'text' class = 'form-control' name = 'Title' style = 'width: 78%;' value='".$Data[0]["Title"] ."' disabled>"); ?>
            </div>

            <div class = 'form-group'>
                <label for  = 'Content'> Content:</label>
                <textarea class='form-control' name='Content' rows='10' required > <?php echo $Data[0]["Content"]; ?></textarea>
            </div>
            <div>
                <input  class = 'btn btn-info' type = 'submit' value = 'Modify' align = 'right' >
            </div>
        </form>
    </div>
</div>

</body>
</html>