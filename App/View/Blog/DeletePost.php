<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/Core/Author.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/Utilities/Error.php';

use ErrorCodes\ErrorCode;
use User\Author;
if(empty($_POST['Id']))
{
	header('Location:'. $_SERVER['DOCUMENT_ROOT'] .'/View/Profile.php?Err=Please Select any Post');
}
else
{
	$UserObj  = new Author();
	$ErrorObj = new ErrorCode();
	$Status = $UserObj->RemovePost($_POST["Id"]);
    header('Location: /View/Profile.php?Err='.$ErrorObj->GetErrorDetail($Status));
}
