<?php

class App {



    function __construct ()
    {

        define("URI", $_SERVER['REQUEST_URI']);
        define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    }

    /**
     *  This function autoload all undefined class of its directory and subdirectories
     *
     */
    function autoload ()
    {
        require_once ROOT.'/vendor/autoload.php';
        require_once ROOT.'/core/config/GoogleAPI.php';
        spl_autoload_register(function ($class) {

            $class = strtolower($class);
            if (file_exists(ROOT . '/core/classes/' . $class . '.php'))
            {

                require_once ROOT . '/core/classes/' . $class . '.php';

            }
        });
    }


    function LoadAndInitializeGoogleAPI()
    {
        require_once ROOT.'/vendor/autoload.php';
        require_once ROOT.'core/config/GoogleAPI.php';
        $GoogleClient = new Google_Client();
        $GoogleClient->setClientId(GoogleClientId);
        $GoogleClient->setClientSecret(GoogleClientSecrets);
        $GoogleClient->setApplicationName('MVCBlogProject');
        $GoogleClient->setRedirectUri('http://www.mvc.com/Authentication/Login');
        $GoogleClient->addScope(
            'https://www.googleapis.com/auth/userinfo.profile 
            https://www.googleapis.com/auth/plus.login 
            https://www.googleapis.com/auth/userinfo.email'
        );
    }

    /**
     *
     * Function Load all the CSS and JS file
     *
     */
    function LoadCssAndJs()
    {
        echo
            "<html lang='en'>".
                "<head>".
                    "<link rel='stylesheet' type='text/css' href='/Public/Css/Header.css'>".
                    "<link rel = 'stylesheet' type='text/css' href = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css'>".
                    "<link rel='stylesheet' type='text/css' href='/Public/Css/Profile.css'>".
                    "<link rel='stylesheet' type='text/css' href='/Public/Css/ViewBlog.css'>".
                    "<script  src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>".
                    "<script  src='/Public/Js/Header.js'></script>".
                    "<script src='/Public/Js/Validation.js'></script>".
                    "<script src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js'></script>".
                "</head>".
            "</html>"
        ;
    }

    function Require ($path)
    {

        require ROOT . $path;

    }

    function start ()
    {

        session_start();

        $Route = explode('/', URI);
        if (file_exists(ROOT . '/App/Controller/' . $Route[1] . '.php'))
        {

            $this->Require('/App/Controller/' . $Route[1] . '.php');
            $controller = new $Route[1]();
        }
        else
        {
            $this->Require('/App/Controller/Main.php');
            $main = new Main();
        }

    }
    
}

?>
