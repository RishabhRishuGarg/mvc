<?php
abstract class Controller
{

    private $Route  = [];
    private $Args   = 0;
    private $Params = [];
    protected $GoogleClient;

    protected $ModelObj = array();


    function __construct ()
    {

        $this->Route = explode('?',URI);
        $this->Route = explode('/', $this->Route[0]);

        $this->Args = count($this->Route);
        $this->GoogleClient = new Google_Client();
        $this->GoogleClient->setClientId(GoogleClientId);
        $this->GoogleClient->setClientSecret(GoogleClientSecret);
        $this->GoogleClient->setApplicationName('MVCBlogProject');
        $this->GoogleClient->setRedirectUri('http://www.mvc.com/GoogleLogin/Index');
        $this->GoogleClient->addScope(
            'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email'
        );
        $this->router();


    }

    private function router ()
    {

        if (class_exists($this->Route[1]))
        {

            if ($this->Args >= 3)
            {
                if (method_exists($this, $this->Route[2]))
                {
                    $this->UriCaller(2, 3);
                }
                else
                {
                    $this->UriCaller(0, 2);
                }
            }
            else
            {
                $this->UriCaller(0, 2);
            }

        }
        else
        {

            if ($this->Args >= 2)
            {
                if (method_exists($this, $this->Route[1]))
                {
                    $this->UriCaller(1, 2);
                }
                else
                {
                    $this->UriCaller(0, 1);
                }
            }
            else
            {
                $this->UriCaller(0, 1);
            }

        }

    }

    private function UriCaller ($method, $param)
    {

        for ($i = $param; $i < $this->Args; $i++)
        {
            $this->Params[$i] = $this->Route[$i];
        }

        if ($method == 0)
            call_user_func_array(array($this, 'Index'), $this->Params);
        else
            call_user_func_array(array($this, $this->Route[$method]), $this->Params);

    }

    abstract function Index ();

    /**
     * Function set the ModelObj array with various demanding class objects.
     *
     * @param $Path
     *
     */
    function Model ($Path)
    {
        $Class = explode('/', $Path);
        $Class = $Class[count($Class)-1];

        if (!class_exists($Class))
        {
            if ( file_exists(ROOT . '/App/Model/' . $Path . '.php') )
            {
                require_once(ROOT . '/App/Model/' . $Path . '.php');
            }
            else
            {
                require_once(ROOT . '/App/Utilities/' . $Path . '.php');
            }
        }
        $this->ModelObj[$Class] = new $Class();


    }

    /**
     *  This function is responsible of getting the View file .
     *
     * @param $path
     * @param array $Data
     *
     * @return void
     */
    function View ($path, $Data = [])
    {

        /*if (is_array($data))
            extract($data);*/

        require(ROOT . '/App/View/' . $path . '.php');

    }

}
